package ex07;

public abstract class FigGeometrica {

	private String color;
	
	
	public FigGeometrica(String c) {
		// TODO Auto-generated constructor stub
		color = c;
	}

	public void mostrarColor(){
		
		System.out.println("El color de la Figura es:"+color);
		
	}
	
	public abstract int area();
	public abstract int perimetro();
	
}
