package ex07;

public class Rectangulo extends FigGeometrica {

	private int ladoA;
	private int ladoB;
	
	public Rectangulo(int A,int B,String c) {
		super(c);
		ladoA = A;
		ladoB = B;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int area() {
		// TODO Auto-generated method stub
		return ladoA*ladoB;
	}

	@Override
	public int perimetro() {
		// TODO Auto-generated method stub
		return ((ladoA*2)+(ladoB*2));
	}

}
