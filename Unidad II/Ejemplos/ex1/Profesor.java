
public class Profesor extends Persona {
	
	private String asignatura;
	
	
	
	public Profesor(String nombre, int edad, float altura, String sexo, String asignatura) {
		super(nombre, edad, altura, sexo);
		this.asignatura = asignatura;
	}

	public void investigar(){
		
		System.out.println("Estoy investigando");
		
	}
	
	public String getAsignatura() {
		return asignatura;
	}


	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}


	public void calificar(){
		
		System.out.println("Estoy calificando");
		
	}
	
}
