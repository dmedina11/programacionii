
public class Alumno extends Persona{
	
	private String carrera;
	
	
	public Alumno(String c,String n,int edad,float altura,String sex){
		
		super(n, edad, altura, sex);
		this.carrera = c;
	
	}
	
	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public void estudiar(){
		
		System.out.println("Estoy estudiando");
		
	}
	

}
