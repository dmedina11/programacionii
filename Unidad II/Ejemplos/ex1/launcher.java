
public class launcher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*Creamos una instancia*/
		Profesor p = new Profesor("Fabian", 30, (float)1.80, "M", "Introducción");
		Alumno a = new Alumno("Ing. en Bioinfo", "Esteban",24, (float)1.90, "M");
		
		/*Accedemos a los datos de la super clase y subclase.*/
		System.out.println(p.getNombre());
		System.out.println(p.getAsignatura());
		
		
		System.out.println(a.getNombre());
		System.out.println(a.getCarrera());
		
		
	}

}
