
public class Animal {
	
	private int edad;
	private String sexo;
	private boolean fertil;
	
	
	public Animal(){
		
		fertil = false;
		
	}
	
	public Animal(int e,String s, boolean f){
		
		edad = e;
		sexo = s;
		fertil = f;
		
	}
	
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public boolean isFertil() {
		return fertil;
	}
	public void setFertil(boolean fertil) {
		this.fertil = fertil;
	}
	
	
	

}
