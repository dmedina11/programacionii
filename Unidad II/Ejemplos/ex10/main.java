
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] a = new int[3];
		Cuenta c = new Cuenta(500);
		
		
		for(int i=0; i < a.length; i++)
			a[i] = i;
		
	
		/* Procedimiento con posible error*/
		try{
			
			for(int i=0; i < 5; i++){
				/*Utilizamos la segunda excepción*/
				//System.out.println("El valor de a: "+a[i]/a[i]);
				
			}
				
			
		}catch(ArrayIndexOutOfBoundsException e){
		/* Captura de la excepción y aquí podemos ejecutar código, sólo si hay error*/	
			
			e.printStackTrace();
			
		}catch(ArithmeticException e){
			
			e.printStackTrace();
			
			
		}finally{
			
		//	System.out.println("Esta print se ejecuta en el bloque finally");
			
		}
		
		
		
		c.deposito(100);
		System.out.println("El balance es: "+ c.getBalance());
		
		
		try {
			c.giro(100);
			System.out.println("El balance es: "+ c.getBalance());
			c.giro(600);
		} catch (bankException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			e.getMonto();
		}
		
		
		
		
		
	}

}
