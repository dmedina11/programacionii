
public class Cuenta {
	
	private double balance;
	
	public Cuenta(double bal){
		
		balance =  bal;
		
	}
	

	public void deposito(double d){
		
		balance+= d;
		
	}
	
	public void giro(double g) throws bankException{
		
		if(balance >= g){
			
			System.out.println("Efectuamos el GIRO por: "+ g);
			balance-=g;
			
		}else{
			
			double valor = (balance - g)*-1;
			throw new bankException(valor);
			
		}
		
	}


	public double getBalance() {
		return balance;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
	
}
