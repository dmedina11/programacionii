
public class Animal {
	
	private String nombre;

	public Animal(String n){
		
		nombre = n;
		
	}
	
	
	public void presentar(){
		
		System.out.println("GRR: "+this.getNombre());
		
	}
	
	public void comer(){
		
		System.out.println("ñam ñam.... comiendo....");
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
