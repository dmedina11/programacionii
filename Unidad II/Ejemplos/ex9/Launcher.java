
public class Launcher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Animal[] animalitos = new Animal[3];
		int aux = 0;
		
		for(int i = 0 ; i < animalitos.length ; i++){
			
			aux = (int)(Math.random()*3+1);
			
			if(aux == 1)
				animalitos[i] =  new Gato("Ramon");
			if(aux == 2)
				animalitos[i] =  new Dinosaurio("Denver");
			if(aux == 3)
				animalitos[i] =  new Conejo("Pepe");
		}
		
	
		for(int i = 0 ; i < animalitos.length ; i++){
			
		
			animalitos[i].presentar();
			
			if(animalitos[i] instanceof Gato){
				Gato g = (Gato) animalitos[i];
				g.cantar();
			}
			
		}
		
		
	
		
	}

}
