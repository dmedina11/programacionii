
public class Animal {
	
	private String nombre;
	private String color;
	private String raza;
	
	
	public Animal(String n,String c,String r){
		
		this.nombre = n;
		this.color = c;
		this.raza = r;
		
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}

}
