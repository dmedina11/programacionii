
public class Vaca extends Animal implements PuedeCorrer{

	
	
	private int leche;
	
	public Vaca(String n,String c,String r,int l){
		super(n,c,r);
		this.leche = l;
	}
	
	@Override
	public void correr() {
		// TODO Auto-generated method stub
		System.out.println("Estoy corriendo");
		
	}
	
	public int getLeche() {
		return leche;
	}

	public void setLeche(int leche) {
		this.leche = leche;
	}

	public void darLeche(int c){
		
		this.leche = this.leche -c;
		System.out.println("Acabo de dar "+c+" y me quedan "+ this.leche+" litros");
		
	}

}
