
public class Gato extends Animal implements PuedeCorrer{

	private int vidas;
	
	public Gato(String n,String c,String r,int vidas){
		
		super(n,c,r);
		this.vidas = vidas;
		
	}

	@Override
	public void correr() {
		// TODO Auto-generated method stub
		System.out.println("Acabo de robar tu comida y ahora corro!!");
	}
	
	public void accidente(){
		
		this.vidas=this.vidas-1;
		
		System.out.println("Ouch acabo de tener un accidente tengo "+this.vidas+" vidas");
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}
	
}
