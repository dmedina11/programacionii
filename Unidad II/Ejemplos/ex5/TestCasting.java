import java.util.ArrayList;


public class TestCasting {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Animal> lista = new ArrayList<Animal>();
		Canario c = new Canario("Piolin","Amarillo","Canario");
		Vaca v = new Vaca("Gloria","Negro con blanco","Clavel",100);
		Gato g = new Gato("Silvestre","Negro","Angora",7);
		
		/*Probamos acceder a los metodos de una subclase.*/
		
		c.cantar();
		
		/*Añadimos el Canario que es un Animal a la lista de Animales*/
		
		lista.add(c);
		
		/*Probamos acceder a los metodos de una subclase.*/
		
		// lista.get(0).cantar(); <--- no se puede , explicación en clases.
		/*Realizamos el cast correspondiente*/
		Canario temp = (Canario)lista.get(0);
		temp.cantar();
		
		/*Añado dos nuevos animales a mi lista de animales*/
		lista.add(v);
		lista.add(g);
		
		/*Realizamos el cast correspondiente*/
		//Canario temp = (Canario)lista.get(1); <--- No se puede, no podemos adivinar 
		//que TIPO de animal se encuentra en la posición 1. Puede ser Canario,Gato o Vaca.
		//temp.cantar();
		
		
		/*Otra técnica.*/
		
		for(int i = 0; i <lista.size();i++){
			
			Animal a = lista.get(i);
			/*Aquí podemos verificar si a que es del tipo Animal
			 * es una instancia de Canario y así sucesivamente.*/
			if(a instanceof Canario){
				((Canario) a).cantar();
			}
			if(a instanceof Vaca){
				System.out.println("Hola Soy la vaca: "+((Vaca)a).getNombre());
				((Vaca) a).correr();
				((Vaca)a).darLeche(1);
			}
			if(a instanceof Gato){
				((Gato) a).accidente();
			}
		}
			
		
		
		
		
		
	}

}
