
public class Launcher {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Droid d = new Droid("Android",100);
		Bender b = new Bender("Bender Rodriguez",100);

		
		
		while(true){
			
			
			System.out.println("--------------------------------------------------");
			System.out.println(d.getNombre() + " HP: "+d.getHp());
			System.out.println(b.getNombre() + " HP: "+b.getHp());
			System.out.println("--------------------------------------------------");
			
			/*Bender parte el combate*/
			int opcion = (int)(Math.random()*2)+1;
			
			if(opcion == 1){
				System.out.println(b.getNombre()+ " esta doblando...");
				b.doblar(d);
			}	
			if(opcion == 2){
				System.out.println(b.getNombre()+ " tiro simple...");
				b.simpleShoot(d);
			}
			
			
			if(b.getHp() <= 0 || d.getHp()<=0){
				break;
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/*Droid ataca*/
			
			
			 opcion = (int)(Math.random()*2)+1;
			
			if(opcion == 1){
				System.out.println(d.getNombre()+ " ataque de misil...");
				d.missileAttack(b);
			}	
			if(opcion == 2){
				System.out.println(d.getNombre()+ " corriendo...");
				d.runFromCombat();
			}
			
			
			if(b.getHp() <= 0 || d.getHp()<=0){
				break;
			}
		}
		
		
		if(d.getHp() >=0)
			System.out.println("El vencedor es: "+d.getNombre());
		else
			System.out.println("El vencedor es: "+b.getNombre());
		
	}
	
}
