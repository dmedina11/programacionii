
public class Robot {

	private String nombre;
	private double hp;
	
	public Robot(String n,int hp){
		
		this.nombre = n;
		this.hp = hp;
		
	}
	
	
	public void healthPower(){
		
			this.hp = (int)Math.random()*100 + this.hp;
		
	}


	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public double getHp() {
		return hp;
	}


	public void setHp(double d) {
		this.hp = d;
	}
	
	
}
