
public class Animal {

	private String tipo;
	private String sexo;
	private String raza;
	
	public Animal(String tipo, String sexo, String raza) {
	
		this.tipo = tipo;
		this.sexo = sexo;
		this.raza = raza;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	

}
