
public class Gato extends Animal {

	private String nick;


	public Gato(String tipo, String sexo, String raza, String nick) {
		super(tipo, sexo, raza);
		this.nick = nick;
	}
	

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	
	
}

