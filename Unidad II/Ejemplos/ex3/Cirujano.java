
public class Cirujano extends Doctor{

	public Cirujano(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}

	public void Operar(){
		
		System.out.println("Estoy operando");
		
	}
	
	/*Overload - Override*/
	public void examinar(){
		
		System.out.println("Estoy examinando ... 10 minutos");
		
	}
	
	public void examinar(String nombre){
		
		System.out.println("Estoy examinando ... a un amigo ("+ nombre  +") 60 minutos");
		
	}
	
	public void examinar(String nombre,int minutos){
		
		System.out.println("Estoy examinando ... a un amigo ("+ nombre  +") "+minutos +" minutos");
		
	}
	
}
