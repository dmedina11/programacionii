
public class launcher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Cirujano c = new Cirujano("Fabian");
		Pediatra p = new Pediatra("Juan");
		
		/*Distintas aplicaciones de un método, overload*/
		c.examinar();
		c.examinar("Camila");
		c.examinar("Pedro", 20);
		
		c.Operar();
		
		p.examinar();
		p.darConsejo();
		
		
	

	}

}
