import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class ventana extends JFrame{

	
	
	public ventana(int h, int w,String title){
		
		this.setSize(h,w);
		this.setTitle(title);
		
	}
	
	public void go(){
		
		
		Pizarra pi = new Pizarra();
		
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.add(pi);
		JLabel nombre = new JLabel("Iñaki Salgado");
		nombre.setFont(new Font("Serif",Font.ITALIC,30));
		nombre.setForeground(Color.YELLOW);
		pi.add(nombre);
		
		this.setVisible(true);
	}
	
}
