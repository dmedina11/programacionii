import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


public class Ventana extends JFrame implements ActionListener{
	
	
	JButton boton,boton2;
	
	DrawPanel panel;

	
	public Ventana(int w, int h, String t){
		
		this.setSize(w,h);
		this.setTitle(t);
		this.setResizable(false);
		/*De esta manera cerramos la ventana*/
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	
	public void go(){
		
		/*Creamos el objeto boton*/
		boton = new JButton("click");
		boton2 = new JButton("shoot");
		panel = new DrawPanel();
	
		
		/*Añadimos el boton al frame*/
		this.setContentPane(panel);
		
		boton.addActionListener(this);
		boton2.addActionListener(new listernerEspecial());
		panel.add(boton);
		panel.add(boton2);
		
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		if(arg0.getSource() == boton){
		System.out.println("Me clickeraron!");
		boton.setText("clicked");
		}
		if(arg0.getSource() == boton2){
			this.repaint();
			
		}
		
	}

}
