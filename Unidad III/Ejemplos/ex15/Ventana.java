import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Ventana extends JFrame{

	
	JButton[] a = new JButton[5];
	
	
	public Ventana(int h,int w,String t){
		
		this.setSize(new Dimension(w,h));
		this.setTitle(t);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	
	public void go(){
		
		for(int i=0; i<a.length;i++){
			a[i]= new JButton(String.valueOf(i));
		}
		JPanel panel = new JPanel();
		
		/*Declarar un layout*/
		BoxLayout bx = new BoxLayout(panel,BoxLayout.X_AXIS);
		/*new BorderLayout();*/
		panel.setLayout(bx);
		
		for(int i=0; i < a.length; i++)
			panel.add(a[i]);
	
		
		this.add(panel);
		
		
		
	}
	
}
