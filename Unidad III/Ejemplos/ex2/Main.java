import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Este widget tiene 4 constructores, y le daremos nombre a la ventana
		JFrame ventana = new JFrame("Mi primera ventana");
		//Asignamos un tamaño a la ventana
		ventana.setSize(200,300);
		//Asignamos una operación por defecto
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Asignamos valor de tamaño de ventana
		ventana.setResizable(true);
		
		//Agremos un widget a la ventana
		ventana.getContentPane().add(new JButton("Clickme"));
		ventana.getContentPane().add(new JTextField(20));
		//Dejamos visible el programa
		ventana.setVisible(true);
		
	}

}
