import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Ventana extends JFrame{

	
	JButton[] a = new JButton[5];
	
	
	public Ventana(int h,int w,String t){
		
		this.setSize(new Dimension(w,h));
		this.setTitle(t);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	
	public void go(){
		
		for(int i=0; i<a.length;i++){
			a[i]= new JButton(String.valueOf(i));
		}
		JPanel panel = new JPanel();
		
		/*Declarar un layout*/
		BorderLayout bl = new BorderLayout(10,5);
		/*new BorderLayout();*/
		panel.setLayout(bl);
		
		panel.add(bl.CENTER,a[0]);
		panel.add(bl.NORTH,a[1]);
		panel.add(bl.EAST,a[2]);
		panel.add(bl.SOUTH,a[3]);
		panel.add(bl.WEST,a[4]);
		
		this.add(panel);
		
		
		
	}
	
}
