import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class BoxLayoutTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Declaramos nuestra ventana*/
			JFrame v = new JFrame("BoxLayout");
			v.setSize(new Dimension(300,300));
			v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			
		/*Declaramos nuestro panel*/

			JPanel panel = new JPanel();
			
			
			BoxLayout bx = new BoxLayout(panel,BoxLayout.X_AXIS);
			//panel.setLayout(bx);
		
			JButton[] arregloBotones = new JButton [5];	
			
			for(int i =0 ; i< 5 ;i++){
				arregloBotones[i]= new JButton(""+i+"");
				arregloBotones[i].setName("Boton"+i+"");
				panel.add(arregloBotones[i]);
			
			}
			for(int i =0 ; i< 5 ;i++){
				System.out.println(arregloBotones[i].getName());
			
			}
		
			v.add(panel);
			v.setVisible(true);
			
			
	}

}
