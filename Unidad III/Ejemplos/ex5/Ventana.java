import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


public class Ventana extends JFrame implements ActionListener{

	
	JButton b1 = new JButton("Avanzar!!!");
	
	public Ventana (String t, int h, int w){
		
		this.setTitle(t);
		this.setSize(h, w);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}
	
	public void go(){
		
		DrawPanel panel = new DrawPanel();
		this.add(panel);
		b1.addActionListener(this);
		this.getContentPane().add(BorderLayout.SOUTH,b1);
		this.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		this.repaint();
		
	}
	
}
