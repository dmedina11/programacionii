import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Este widget tiene 4 constructores, y le daremos nombre a la ventana
		JFrame ventana = new JFrame("Mi primera ventana");
		
		JPanel miPanel = new JPanel();
		//Asignamos un tamaño a la ventana
		ventana.setSize(200,300);
		//Asignamos una operación por defecto
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Asignamos valor de tamaño de ventana
		ventana.setResizable(true);
		
		//Asignamos el panel a la ventana
		ventana.add(miPanel);
		
		//Agremos un widget a la ventana
		miPanel.add(new JButton("Clickme"));
		miPanel.add(new JTextField(20));
		//Dejamos visible el programa
		ventana.setVisible(true);
		
	}

}
