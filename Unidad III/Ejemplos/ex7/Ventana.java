import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;


public class Ventana extends JFrame{

	JTextArea text;
	int contador =0;
	
	public Ventana(String t, int h, int w){
		
		this.setTitle(t);
		this.setSize(w, h);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	
	
	public void go(){
		
		
		JPanel panel = new JPanel();
		JButton boton = new JButton("Click");
		JButton boton2 = new JButton("Clack");
		text = new JTextArea(20,40);
		JScrollPane bar = new JScrollPane(text);
		
		bar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		bar.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		text.setText("Escriba aquí!");
		text.selectAll();
		text.requestFocus();
		
		panel.add(bar);
		this.getContentPane().add(BorderLayout.CENTER,panel);
		this.getContentPane().add(BorderLayout.SOUTH,boton);
		this.getContentPane().add(BorderLayout.EAST,boton2);
		boton.addActionListener(new printArea());
		boton2.addActionListener(new Clack());
		this.setVisible(true);
		
	}

	public class printArea implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			text.append("Clicked" + contador+ "\n");
			contador ++;
		}
		
		
		
	}

	public class Clack implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			text.append("Clack" + contador + "\n");
			contador--;
		}
		
		
	}

	
}

