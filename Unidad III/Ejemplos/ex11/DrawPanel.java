import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;


public class DrawPanel extends JPanel{
	
	
	public void paintComponent(Graphics g){
		
		
		int r = (int)(Math.random()*255);
		int gr = (int)(Math.random()*255);
		int b = (int)(Math.random()*255);
		
		Color randomColor = new Color(r,gr,b);
		
		
		g.setColor(randomColor);
		g.fillOval(20, 20, 200, 200);
		
		
	}

}
