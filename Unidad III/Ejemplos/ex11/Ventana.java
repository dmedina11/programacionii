import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Ventana extends JFrame{
	
	
	JButton boton;
	JLabel label;
	DrawPanel panel;
	JTextField text;
	
	public Ventana(int w, int h, String t){
		
		this.setSize(w,h);
		this.setTitle(t);
		this.setResizable(false);
		/*De esta manera cerramos la ventana*/
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	
	public void go(){
		
		/*Creamos el objeto boton*/
		boton = new JButton("click");
		/*string*/
		label = new JLabel("Ingrese su nombre");
		/*Textfield*/
		text = new JTextField(20);
		
		panel = new DrawPanel();
	
		
		/*Añadimos el boton al frame*/
		this.setContentPane(panel);
		panel.add(label);
		panel.add(text);
		panel.add(boton);
		
	}

}
