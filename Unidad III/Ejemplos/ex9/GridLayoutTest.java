import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GridLayoutTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		/*Declaramos nuestra ventana*/
		JFrame v = new JFrame("GridLayput");
		v.setSize(new Dimension(300,300));
		v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	/*Declaramos nuestro panel*/

		JPanel panel = new JPanel();
		JButton a = new JButton("North");
		
		/*Cuando dejamos un 0, significa que podremos agregar N Filas*/
		//GridLayout gl = new GridLayout(0,3);
		//GridLayout gl = new GridLayout(0,3,2,15);
		GridLayout gl = new GridLayout(3,3);
		
		panel.setLayout(gl);
	
		JButton[] arregloBotones = new JButton [9];	
		
		for(int i =0 ; i< 9 ;i++){
			arregloBotones[i]= new JButton(""+i+"");
			arregloBotones[i].setName("Boton"+i+"");
			panel.add(arregloBotones[i]);
		
		}
		for(int i =0 ; i< 5 ;i++){
			System.out.println(arregloBotones[i].getName());
		
		}
	
		v.getContentPane().add(BorderLayout.NORTH,a);
		v.getContentPane().add(BorderLayout.CENTER,panel);
		v.setVisible(true);
		
		

	}

}
