import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class BorderLayoutTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Declaramos nuestra ventana*/
			JFrame v = new JFrame("BorderLayout");
			v.setSize(new Dimension(300,300));
			v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			
		/*Declaramos nuestro panel*/

			JPanel panel = new JPanel();
			BorderLayout bl = new BorderLayout(5,15);
			panel.setLayout(bl);
			
			JButton[] arregloBotones = new JButton [5];	
			
			for(int i =0 ; i< 5 ;i++){
				arregloBotones[i]= new JButton(""+i+"");
				
			
			}
			panel.add(bl.CENTER,arregloBotones[0]);
			panel.add(bl.WEST,arregloBotones[1]);
			panel.add(bl.EAST,arregloBotones[2]);
			panel.add(bl.NORTH,arregloBotones[3]);
			panel.add(bl.SOUTH,arregloBotones[4]);
			v.add(panel);
			v.setVisible(true);
			
			
	}

}
