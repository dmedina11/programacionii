import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;


public class MiApplet extends Applet{

	
	
	public void init(){
			
		
		Color c = Color.PINK;
		this.setSize(600,600);
		this.setBackground(c);
		
	}
	
	// Es un método que implementa la API - AWT.
	public void paint(Graphics g){
		
		g.drawLine(0, 300, 600,300);
		g.drawString("-X", 10, 290);
		g.drawLine(300,0, 300,600);
		g.drawString("X", 590, 290);
		g.drawString("Y", 310, 10);
		g.drawString("-Y", 310, 580);
		g.drawString("I", 460, 150);
		
		g.drawRect(400, 20, 100,200);
		
		g.setColor(Color.MAGENTA);
		g.fillRect(420,30, 100, 200);
		
		g.setColor(Color.RED);
		g.fillRect(450,50, 100, 200);
		
		g.setColor(Color.YELLOW);
		g.fillOval(0, 0, 300,300);
		
		g.setColor(Color.ORANGE);
		g.fillOval(0, 310, 200,200);
	
		
	}
	
	
}
