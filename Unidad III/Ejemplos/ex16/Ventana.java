import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Ventana extends JFrame{

	
	JButton[] a = new JButton[9];
	
	
	public Ventana(int h,int w,String t){
		
		this.setSize(new Dimension(w,h));
		this.setTitle(t);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	
	public void go(){
		
		for(int i=0; i<a.length;i++){
			a[i]= new JButton(String.valueOf(i));
		}
		JPanel panel = new JPanel();
		
		/*Declarar un layout*/
		GridLayout gl = new GridLayout(3,3,5,5);
		/*new GridLayout(n,m,w,l);*/
		panel.setLayout(gl);
		
		for(int i=0; i < a.length; i++){
			panel.add(a[i]);
			a[i].addActionListener(new listerner());
		}
		
		this.add(panel);
		
		
		
	}
	
	public class listerner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			
			if(arg0.getSource() == a[1]){
				
				int cont = 0;
				
				while(true){
					 cont = 0;
					 
					for(int i=0; i< a.length;i++)
						a[i].setText(String.valueOf((int)(Math.random()*9)));
					
					for (int i=0; i< a.length; i++)
						for(int j=i+1; j<a.length;j++){
							if(Integer.parseInt(a[i].getText()) == Integer.parseInt(a[j].getText()) )
								cont++;
							
						}	
					if(cont == 0)
						break;
				}
			
			
			}

		}
	}
	
}
