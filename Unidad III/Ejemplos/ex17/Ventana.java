import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;


public class Ventana extends JFrame{

	
	
	
	public Ventana(int h,int w,String t){
		
		this.setSize(new Dimension(w,h));
		this.setTitle(t);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	
	public void go(){
		
		JTextArea text = new JTextArea(10,30);
		JPanel panel = new JPanel();
		JScrollPane bar = new JScrollPane(text,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//bar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		//bar.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel.add(bar);
		
		text.setText("Ingrese el texto");
		text.selectAll();
		
		
		this.getContentPane().add(new JLabel("Se viene el proyecto!"),BorderLayout.NORTH);
		this.getContentPane().add(new JButton("Grabar"),BorderLayout.SOUTH);
		
		this.add(panel);
	}
	
}
