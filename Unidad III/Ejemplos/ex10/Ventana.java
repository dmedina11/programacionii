import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Ventana extends JFrame {

	public int x=0;
	public int y=0;
	
	
	public Ventana(String s,int w, int h){
		
		super(s);
		this.setSize(w,h);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	
	
	public void go() {
		// TODO Auto-generated method stub
		
		DrawPanel panel = new DrawPanel();
		this.add(panel);
	
		
		for(int i=0; i< 500; i++){
			
			try{
				
				Thread.sleep(3);
				x++;
				y++;
				panel.repaint();
				
			}catch(Exception e){}
			
			
		}
		
	
	
	}

	
	public class DrawPanel extends JPanel{
		
		public void paintComponent(Graphics g){
			
			/*Vuelvo al estado anterior*/
			g.setColor(Color.WHITE);
			g.fillRect(0,0, this.getWidth(),this.getHeight());
			g.setColor(Color.BLACK);
			g.drawRect(40, 40, 300,300);
			
			/*Dibujo mi bolita*/
			g.setColor(Color.BLUE);
			g.fillOval(x, y,30, 30);
			
		}
		
	}
	
	
	
}
