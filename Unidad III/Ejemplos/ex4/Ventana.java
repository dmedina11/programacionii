import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;


public class Ventana extends JFrame implements ActionListener{

	
	JButton b1 = new JButton("SUR");
	JButton b2 =  new JButton("Norte");
	JTextArea t1 = new JTextArea();
	JLabel label = new JLabel("Escriba un texto:");
	JButton b3 = new JButton("<----");
	int contador = 0;
	public Ventana(String t,int h,int w){
		
		this.setTitle(t);
		this.setSize(w, h);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public void go(){
		
		/*Añadimos el listener al boton b2*/
		b2.addActionListener(this);
		b1.addActionListener(this);
		this.getContentPane().add(BorderLayout.SOUTH,b1);
		this.getContentPane().add(BorderLayout.NORTH,b2);
		this.getContentPane().add(BorderLayout.CENTER,t1);
		this.getContentPane().add(BorderLayout.WEST,label);
		this.getContentPane().add(BorderLayout.EAST,b3);
		this.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		
		
		/*Con el if, somos capaces de poder discriminar desde donde proviene el evento.
		 * Sin embargo esta no es la mejor forma de trabajar!, mañana veremos el por qué!*/
		if(e.getSource() == b1){
		 	
			b1.setName("Clicked");
			b1.setBackground(Color.CYAN);
		}
		if(e.getSource() == b2){
			
			t1.setText("ESCRIBA AQUI!!!! AHORA!!!");
			b1.setBackground(Color.MAGENTA);
		}
		/* Este codigo siempre se ejecutará cuando se realice un evento* */
		System.out.println("Me acaban de pinchar!!! "+contador+" veces");
		contador++;
		
	}
	
	
}
