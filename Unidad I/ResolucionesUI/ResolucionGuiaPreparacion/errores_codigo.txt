Class book{
	String title;
	String author;
}

Class BooksTestDrive{
	public static void main (String [] args){
		books[] mybooks = new Books[3];
		int x=0;
		mybooks[0].title=”the grapes of java”;
		mybooks[1].title=”the java gatsby”;
		mybooks[2].title=”the java cookbook”;
		mybokks[0].author = “bob”;
		mybokks[1].author = “sue”;
		mybokks[2].author = “ian”;

		while( x < 3){
			System.out.print(mybooks[x].title);
			System.out.print(“ by ”);
			System.out.print(mybooks[x].author);
			x = x+1;
		}
	}
}

Errores:

1. Las clases se definen con class no Class
2. La definicion del arreglo es book [] no books[], esa clase no existe, ademas que en la instancia de espacio esta escrito con Books y 
la clase se llama book, deberia ser:

	book[] mybooks = new book[3];
3. Cuando accede a las posiciones arrojara un java.lang.nullpointerexception, esto es debido a que esta accediendo a objetos en posicion null
Esto sucedera cada vez que desee acceder a un elemento de la coleccion, dado a que nunca se ha instanciado un libro, si no que una colección
con espacio para almacenar libros (book object)
