package carrera;

public class Launcher {

	//se define con un random (1: 15; 2: 20; 3; 30)
	public static int DefinirTramo(){
		
		int random = (int)(Math.random()*3)+1;
		int tramo=0;
		switch (random){
			case 1:
				tramo=15;
				break;
			case 2:
				tramo=20;
				break;
			case 3:
				tramo=30;
				break;
			default:
				tramo=15;
				break;
		}
		
		return tramo;
	}
	
	public static void main (String [] args){
		
		Carrera carrera = new Carrera(DefinirTramo(), 2);//tramo random con dos ranas
		//inscribimos ranas
		carrera.InscribirRamas(new Rana("rojo", "david"));
		carrera.InscribirRamas(new Rana("verde", "Juaki"));
		
		//simular carrera...
		carrera.SimularCarrera();
		//mostrar ganador
		System.out.println("Color rana ganadora: " + carrera.getColor_ranaGandora());
		
	}
}
