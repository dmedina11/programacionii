package ex8;

public class TestTaximetro {

	public static void main (String [] args){
		
		//objeto taximetro...
		Taximetro taxi = new Taximetro(300);//se prueba con 300 como bajada de bandera
		
		int termino = (int)(Math.random()*10)+1;//para hacer la simulacion---
		
		for (int i=1; i<=termino; i++){
			
			taxi.SimulaAvance();//simulamos el viaje...
			try{
				Thread.sleep(3000);//que pase 3 segundos
			}catch(InterruptedException e){
				
			}
		}
		
		System.out.println("Fin de Viaje...");
		taxi.getPrecioFinal();
		taxi.resumenViaje();
	}
}
