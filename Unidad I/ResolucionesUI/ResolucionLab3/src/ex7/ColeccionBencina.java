package ex7;

//representa la coleccion de bencina por todas las semanas...
public class ColeccionBencina {

	//atributos...
	private Bencina [] conjunto_bencina;//es un array
	
	//constructor...
	public ColeccionBencina(){
		
		//instancia a la coleccion
		this.conjunto_bencina = new Bencina[52];//son 52 semanas
		
		//agrego objetos a la coleccion...
		for (int i=0; i<52; i++){
			this.conjunto_bencina[i] = new Bencina(i);
		}
	}
	
	//metodo para modificar el precio de una semana de la bencina...
	public void setPrecionporSemana(int semana, double precio){
		
		this.conjunto_bencina[semana].setPrecio(precio);//modificamos el precio de la bencina dada su posicion
	}
	
	//metodo que permite obtener la semana con el mayor precio...
	public int semanaMayor(){
		
		//variables de contenido
		int semana_mayor=0;
		double precio_mayor=this.conjunto_bencina[0].getPrecio();
		
		//recorrer array
		for (int i=1; i<this.conjunto_bencina.length; i++){
			
			if (precio_mayor<this.conjunto_bencina[i].getPrecio()){
				precio_mayor=this.conjunto_bencina[i].getPrecio();
				semana_mayor=i;
			}
		}
		
		return semana_mayor;
	}
	
	//metodo que permite obtener la semana con el menor precio...
	public int semanaMenor(){
		
		//variables de contenido
		int semana_menor=0;
		double precio_menor=this.conjunto_bencina[0].getPrecio();
		
		//recorrer array
		for (int i=1; i<this.conjunto_bencina.length; i++){
			
			if (precio_menor>this.conjunto_bencina[i].getPrecio()){
				precio_menor=this.conjunto_bencina[i].getPrecio();
				semana_menor=i;
			}
		}
		
		return semana_menor;
	}
	
	//metodo que permite calcular el promedio de todas las semanas sin considerar los 0
	public double promedioSemanas(){
		
		double prom=0;
		int cont=0;//para los que son distintos de 0
		
		//recorremos...
		for (int i=0; i<this.conjunto_bencina.length; i++){
			
			if (this.conjunto_bencina[i].getPrecio()>0){//aculamos monto...
				cont++;
				prom=prom+this.conjunto_bencina[i].getPrecio();
			}
		}
		
		return prom/cont;//retornamos el promedio, la suma de los precios dividido por la cantidad de no 0
	}

	public Bencina[] getConjunto_bencina() {
		return conjunto_bencina;
	}
}