//clase para testear los metodos de los ejercicios 1 al 6
public class TestExercises {

	public static void main (String [] args){
		
		//instancia a WorksDataValues
		WorksDataValues works = new WorksDataValues();
		System.out.println("Ex 01 " + works.CompareNumbers(4, 2));
		
		//test arrays...
		works.printArray(works.createArray());
		
		//test ex5...
		works.setArray(works.createArray(), 10);
		
		//test ex6...
		works.printArray(works.ConcatenateArray(works.createArray(), works.createArray()));
	}
}
