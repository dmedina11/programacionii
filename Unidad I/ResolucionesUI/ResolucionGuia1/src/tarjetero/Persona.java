package tarjetero;

public class Persona {

	private String nombre;
	private String apellidos;
	private int telefono;
	private String correo;
	private String postal;
	
	public Persona(String nombre, String apellidos, int telefono, String correo, String postal) {
	
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.correo = correo;
		this.postal = postal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}
	
	//para mostrar con formato
	public void MostrarDatos(){
		System.out.println("--------------------------------------------");
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Apellidos: " + this.apellidos);
		System.out.println("Telefono: " + this.telefono);
		System.out.println("Correo: " + this.correo);
		System.out.println("Direccion Postal: " + this.postal);
		System.out.println("--------------------------------------------");
	}
}
