package exe_sencillos;

import java.util.Scanner;

public class Exe_5_7 {

	public static void Suma(int num1, int num2){
		
		int resultado = num1 + num2;
		System.out.println(num1 +" + "+ num2 + " = " + resultado);
	}
	
	public static void Solicitud(){
		
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("Ingresa numero 1 a sumar: ");
		int num1 = leer.nextInt();
		System.out.println("Ingresa numero 2 a sumar: ");
		int num2 = leer.nextInt();
		Suma(num1, num2);
	}
	
	public static void main (String [] args){
		
		//exe5
		Suma(5, 7);
		
		//exe7
		Solicitud();
	}
}
