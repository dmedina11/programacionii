package exe_sencillos;

import java.util.Scanner;

public class Exe10 {
	
	public static void Conversor(String numero){
		
		int [] digitos = new int[4];//para obtener los digitos
		char [] numeros = numero.toCharArray();
		for (int i=0; i<4; i++){
			digitos[i] = (int)numeros[i];
			digitos[i] = (digitos[i]+7)%10;
		}
		
		System.out.println("Input: " + numero);
		System.out.println("Output: " + digitos[2] + digitos[3]+ digitos[0]+ digitos[1]);
	}
	
	public static void main (String [] args){
		
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("Ingrese el numero");
		int num = leer.nextInt();
		
		//conversion a String...
		String numero = Integer.toString(num);
		
		if (numero.length()!= 4)
			System.out.println("Numero con cantidad de digitos incorrecta");
		else{
			Conversor(numero);
		}
	}
}
