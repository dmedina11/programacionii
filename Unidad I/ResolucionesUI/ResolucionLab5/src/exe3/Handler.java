package exe3;

import java.util.Scanner;

//clase que tiene la responsabilidad de controlar las acciones con respecto al Container...
public class Handler {

	private  Scanner leer;//para leer desde la entrada estandar...
	
	public Handler(){
		
		this.leer = new Scanner(System.in);//instancia al lector...
	}
	
	//muestra el menu y captura la opcion recogida desde la entrada estandar
	public int Menu(){
		
		int opcion=0;
		
		System.out.println("<1> Ingresar Ficha");
		System.out.println("<2> Buscar ficha por nombre de paciente");
		System.out.println("<3> Buscar ficha por peso de paciente");
		System.out.println("<4> Buscar ficha por altura de paciente");
		System.out.println("<5> Salir");
		System.out.println(">> ");
		opcion = this.leer.nextInt();
		return opcion;
	}
	
	//metodo que permite generar el control de las opciones...
	public void ManagerOption(Container container){
		
		//ciclo infinito
		while (true){
			
			int option = this.Menu();
			
			switch (option){
			
				case 1:
					//ingresamos una ficha...
					container.IngresarFicha(this.CrearFicha());
					break;
				case 2:
					//se busca una ficha por nombre...
					System.out.println("Complete los datos de la ficha para buscar por nombre");
					container.BuscarPorNombre(this.CrearFichaBN());
					break;
				case 3:
					//se busca una ficha por peso...
					System.out.println("Complete los datos de la ficha para buscar por peso");
					container.BuscarPorPeso(this.CrearFichaBP());
					break;
				case 4:
					//se busca una ficha por nombre...
					System.out.println("Complete los datos de la ficha para buscar por altura");
					container.BuscarPorAltura(this.CrearFichaBA());
					break;
				case 5:
					System.out.println("Adios!!!");
					System.exit(0);
					break;
				default:
					System.out.println("Ingresa una opcion valida");
			}
		}
	}
	
	//metodo que permite crear una ficha...
	private FichaPaciente CrearFicha(){
		
		//solicitud de datos...
		System.out.println("Ingrese nombre paciente");
		String nombre = this.leer.next();
		System.out.println("Ingrese peso del paciente");
		double peso = this.leer.nextDouble();
		System.out.println("Ingrese altura del paciente");
		double altura = this.leer.nextDouble();
		System.out.println("Ingrese tipo de sangre");
		String tipo_sangre = this.leer.next();
		
		//retornar una nueva ficha...
		return new FichaPaciente(nombre, peso, altura, tipo_sangre);	
	}
	
	//metodo que permite crear una ficha...
	private FichaPaciente CrearFichaBP(){
			
		//solicitud de datos, solo el peso, el resto es vacio
		System.out.println("Ingrese peso del paciente");
		double peso = this.leer.nextDouble();
		
		//retornar una nueva ficha...
		return new FichaPaciente("", peso, 0, "");	
	}

	//metodo que permite crear una ficha...
	private FichaPaciente CrearFichaBN(){
				
		//solicitud de datos, solo el peso, el resto es vacio
		System.out.println("Ingrese nombre del paciente");
		String nombre = this.leer.next();
			
		//retornar una nueva ficha...
		return new FichaPaciente(nombre, 0, 0, "");	
	}
	//metodo que permite crear una ficha...
	private FichaPaciente CrearFichaBA(){
			
		//solicitud de datos, solo la altura, el resto es vacio...
		System.out.println("Ingrese altura del paciente");
		double altura = this.leer.nextDouble();
		
		//retornar una nueva ficha...
		return new FichaPaciente("", 0, altura, "");	
	}
}
