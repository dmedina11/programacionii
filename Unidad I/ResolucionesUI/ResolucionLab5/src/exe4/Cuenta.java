package exe4;

//clase con la responsabilidad de almacenar la informacion de una cuenta
public class Cuenta {

	//atributos de la clase
	private int numero;
	private int saldo;
	
	//constructor de la clase...
	public Cuenta(int numero, int saldo) {
		
		this.numero = numero;
		this.saldo = saldo;
	}
	
	//consultar el saldo...
	public void ConsultarSaldo(){
		
		System.out.println("Saldo actual en cuenta: " + this.numero + " es: " + this.saldo);
	}
	
	//abonar a la cuenta...
	public void Abonar(int abono){
		
		System.out.println("Abono de: " + abono + " a cuenta " + this.numero);
		this.saldo = this.saldo+abono;
	}
	
	//pagar con la cuenta...
	public void Pago(int pago){
		
		System.out.println("Realizando pago de: " + pago + " con la cuenta " + this.numero);
		
		this.saldo = this.saldo -pago;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
}