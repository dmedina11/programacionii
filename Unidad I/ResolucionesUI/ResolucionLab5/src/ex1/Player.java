package ex1;

import java.util.Scanner;

//clase que tiene la responsabilidad de generar los atributos y metodos del computador...
public class Player {

	private int number;
	private String name;
	private int kind;
	
	//constructor de la clase...
	public Player(String name, int kind) {
		
		this.name = name;//asignamos valor al nombre del jugador
		this.kind = kind;//para determinar el tipo de jugador, si es player o pc (0 PC, 1 Player)
	}
	
	//metodo para generar el numero del computador
	public void generateNumberPC(){
		
		this.number = (int)(Math.random()*10)+1;//sera un numero random entre 1 y 10
	}

	//metodo para asignar el numero de los jugadores...
	public void generateNumberPlayer(){
		
		System.out.println(this.name + " ingrese su numero");
		//para la lectura de teclado	
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		this.number = scanner.nextInt();//asignamos valor al atributo
	}
	
	//getter and setter
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getKind(){
		return kind;
	}
}