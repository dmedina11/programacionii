package ex2;

//clase que genera las instancias y las llamadas a los metodos correspondientes
public class Launcher {

	public static void main (String [] args){
		
		Termometro term = new Termometro();//instancia a termometro...
		
		//agitamos
		term.agitar();
		term.MostrarTemperatura();
		
		//medimos...
		term.Medir();
		term.MostrarTemperatura();
	}
}
