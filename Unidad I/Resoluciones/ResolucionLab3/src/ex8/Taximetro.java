package ex8;

//clase con la informacion del taximetro
public class Taximetro {

	private int bajada;
	private double tiempo;
	private double total_viaje;
	private double distancia;
	
	//constructor de la clase...
	public Taximetro(int bajada){
		
		this.bajada = bajada;
		this.tiempo = 0;
		this.total_viaje=0;
		this.distancia = 0;
	}
	
	//simular avance....
	public void SimulaAvance(){
		
		double avance = (Math.random()*100)+1;
		double recorrido = (Math.random()*100)+1 + avance;
		this.tiempo = this.tiempo+avance;
		this.distancia = this.distancia+recorrido;
		System.out.println("Tiempo Actual: " + this.tiempo);
		System.out.println("Distancia Actual: " + this.distancia);
	}
	
	//estimar precio final...
	public void getPrecioFinal(){
		
		this.total_viaje = this.bajada + (this.distancia*150) + (this.distancia*60);
	}
	
	//resumen de viaje...
	public void resumenViaje(){
		System.out.println("---------------------------------");
		System.out.println("Bajada de Bandera: " + this.bajada);
		System.out.println("Tiempo total: " + this.tiempo);
		System.out.println("Distancia total: " + this.distancia);
		System.out.println("Monto total: " + this.total_viaje);
		System.out.println("---------------------------------");
	}
}
