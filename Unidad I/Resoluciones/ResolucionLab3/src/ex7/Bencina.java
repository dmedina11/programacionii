package ex7;

//representa la bencina con su precio y la semana a la que corresponde
public class Bencina {

	private int semana;
	private double precio;
	
	//constructor de la clase...
	public Bencina(int semana){
		this.precio =0;
		this.semana=semana;
	}

	//getters and setters
	public int getSemana() {
		return semana;
	}

	public void setSemana(int semana) {
		this.semana = semana;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getPrecio() {
		return precio;
	}
}
