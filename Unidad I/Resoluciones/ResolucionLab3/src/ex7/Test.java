package ex7;

import java.util.Scanner;

//clase que permite probar los metodos...
public class Test {

	//metodo que permite tener el control de la llamada a los metodos segun la opcion dada...
	public static void Control(int opcion, ColeccionBencina coleccion, Scanner leer){
		
		switch (opcion){
		
			case 1:
				System.out.println("Ingrese semana (0 - 51)");
				int semana = leer.nextInt();//no esta validado...
				System.out.println("Ingrese precio de la semana");
				double precio = leer.nextDouble();//no esta validado...
				coleccion.setPrecionporSemana(semana, precio);
				
				break;
			case 2:
				System.out.println("Promedio de precios: " + coleccion.promedioSemanas());
				break;

			case 3:
				int mayor = coleccion.semanaMayor();
				System.out.println("Precio mayor en la semana: " + mayor);
				System.out.println("Con un valor de: " + coleccion.getConjunto_bencina()[mayor].getPrecio());
				break;

			case 4:
				int menor = coleccion.semanaMenor();
				System.out.println("Precio menor en la semana: " + menor);
				System.out.println("Con un valor de: " + coleccion.getConjunto_bencina()[menor].getPrecio());
				
				break;

			case 5:
				System.out.println("Adios!!!");
				break;
				
			default:
				System.out.println("Opcion incorrecta...");
				break;

		}
	}
	
	public static void main(String [] args){
		
		//instancia de ColeccionBencina
		ColeccionBencina coleccion = new ColeccionBencina();
		Scanner leer = new Scanner (System.in);//para leer por teclado
		int opcion=0;
		
		//hacemos un menu...
		do{
			System.out.println("<1> Ingresar precio bencina");
			System.out.println("<2> Consultar promedio bencina");
			System.out.println("<3> Consultar semana con precio mayor");
			System.out.println("<4> Consultar semana con precio menor");
			System.out.println("<5> Salir");
			System.out.println(">> ");
			opcion = leer.nextInt();
			Control(opcion, coleccion, leer);
		}while (opcion != 5);
		
	}
}
