Respuestas a preguntas

¿Qué es un estado de un objeto?

Son los atributos que puede tener un objeto en un instante dado, las características y sus cualidades

¿Cuantos comportamientos puede tener un objeto?

N comportamientos

¿Cuando hablamos de encapsulamiento... a qué nos referimos?

Un objeto es dueño de sus propios estados y comportamientos

¿Cúal es la principal diferencia entre los conceptos de asociación y composición?

Composición es parte de una relación de asociación, la diferencia es el enfoque, debido a que una clase con composición no tiene
sentido que exista si no existe la clase por la que está compuesta, lo que no sucede con la asociación

¿Cómo se comunican los objetos?

Se comunican mediante el paso de mensajes a través de sus métodos.


