package carrera;

public class Rana {

	private String color;
	private int distancia;
	private String dueno;
	
	//contructor de la clase...
	public Rana(String color, String dueno) {
	
		this.color = color;
		this.dueno = dueno;
		this.distancia = 0;//la distancia inicial es 0
	}
	
	//simular el salto
	public void Salto(){
		
		int salto = (int)(Math.random()*3)+1;//+1 para que no salga 0
		System.out.println("Rana: " + this.color + " de: " + this.dueno + " salto: " + salto +" metros");
		this.distancia = this.distancia+salto;
	}

	//metodos para conocer la informacion...
	public String getColor() {
		return color;
	}
	
	public int getDistancia() {
		return distancia;
	}

	public String getDueno() {
		return dueno;
	}
}