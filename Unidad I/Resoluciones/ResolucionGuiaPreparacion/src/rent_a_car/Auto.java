package rent_a_car;

public class Auto {

	//atributos
	private String marca;
	private int capacidad;
	private int KM;
	private int posicion;
	
	//constructor de la clase...
	public Auto(String marca, int capacidad, int posicion){
		this.marca = marca;
		this.capacidad = capacidad;
		this.KM = (int)(Math.random()*2000)+1;//generar numero random...
		this.posicion = posicion;
	}

	//getter and setter
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public int getKM() {
		return KM;
	}

	public void setKM(int kM) {
		KM = kM;
	}
	
	public int getPosicion(){
		return posicion;
	}
}