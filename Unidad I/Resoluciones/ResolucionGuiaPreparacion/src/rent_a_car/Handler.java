package rent_a_car;

import java.util.Scanner;

//clase con el menu de opciones
public class Handler {

	//metodo para devolver un auto...
	public Auto DevolverAuto (Auto [] arrendados){
		
		System.out.println("Ingrese numero de espacio para devolver el auto (1 : 10)");
		@SuppressWarnings("resource")
		int espacio = new Scanner(System.in).nextInt();
		Auto a_devolver=null;
		
		//buscamos el espacio entre los autos...
		for (int i=0; i<arrendados.length; i++){
			if (arrendados[i] != null){
				if (arrendados[i].getPosicion() == espacio){
					a_devolver=arrendados[i];
					break;
				}
			}
		}
		return a_devolver;
	}
	
	//menu para seleccionar una opcion
	@SuppressWarnings("resource")
	public int Menu(){
	
		int opcion;
		System.out.println("<1> Arrendar Auto");
		System.out.println("<2> Devolver Auto");
		System.out.println("<3> Iniciar Sistema");
		System.out.println("<4> Ver Autos Disponibles");
		System.out.println("<5> Ver Autos Arrendados");
		System.out.println("<6> Salir");
		
		System.out.println(">> ");
		opcion = new Scanner(System.in).nextInt();
		return opcion;
	}
	
	//metodo manager
	public void Manager(Garage garage){
		
		Auto [] arrendados = new Auto[10];//para contener los autos arrendados...
		int i=0;//para el avance de autos arrendados...
		while(true){
			int opcion = this.Menu();
			
			switch (opcion){
				
				case 1:
					if (i<10){//me podria salir del arreglo
						arrendados[i]=garage.Arrendar();
						i++;
					}
					break;
				case 2:
					garage.EntregarAuto(this.DevolverAuto(arrendados));
					break;
				case 3:
					System.out.println("Iniciando sistema");
					garage.IniciarSistema();
					break;
				case 4:
					System.out.println("Autos Disponibles");
					garage.MostrarAutos(garage.getEspacio());
					break;
				case 5:
					System.out.println("Autos Arrendados");
					garage.MostrarAutos(arrendados);
					break;
				case 6:
					System.out.println("Adios!!!");
					System.exit(0);
					break;
				default:
					System.out.println("Ingresa una nueva opcion");
					break;
			}
		}
	}
}
