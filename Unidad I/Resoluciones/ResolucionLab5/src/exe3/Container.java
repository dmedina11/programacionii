package exe3;

//clase con la responsabilidad de tener una coleccio de fichas
public class Container {

	//atributos de la clase...
	private FichaPaciente [] Fichas;
	
	//constructor de la clase
	public Container() {
		
		//instancia a la coleccion...
		this.Fichas = new FichaPaciente[20];
	}
	
	//metodo para buscar una ficha por nombre...
	public void BuscarPorNombre(FichaPaciente ficha){
		
		int cont=0;//para determinar si no se encontro ninguna ficha con el nombre dado
		
		//recorremos la coleccion
		for (int i=0; i<this.Fichas.length; i++){
			
			if (this.Fichas[i] != null){//evaluamos que la posicion no es vacia y comparamos los nombres llamando al metodo que muestra la informacion
				if (this.Fichas[i].getNombre().equalsIgnoreCase(ficha.getNombre())){
					this.Fichas[i].MostrarFicha();
					cont=1;
					break;
				}
			}
		}
		
		if (cont==0)
			System.out.println("No se ha encontrado ninguna ficha en el container con el nombre de " + ficha.getNombre());
	}
	
	//metodo para buscar por peso
	public void BuscarPorPeso(FichaPaciente ficha){
		
		int cont=0;//para determinar si no se encontro ninguna ficha con el nombre dado
		
		//recorremos la coleccion, no tiene break porque pueden haber mas de una que cumpla con las caracteristicas
		for (int i=0; i<this.Fichas.length; i++){
			
			if (this.Fichas[i] != null){//evaluamos que la posicion no es vacia y comparamos los pesos llamando al metodo que muestra la informacion
				if (this.Fichas[i].getPeso()< ficha.getPeso()){
					this.Fichas[i].MostrarFicha();
					cont=1;
				}
			}
		}
		
		if (cont==0)
			System.out.println("No se ha encontrado ninguna ficha en el container con peso menor a " + ficha.getPeso());
	}
	
	//metodo para buscar por peso
	public void BuscarPorAltura(FichaPaciente ficha){
		
		int cont=0;//para determinar si no se encontro ninguna ficha con el nombre dado
		
		//recorremos la coleccion, no tiene break porque pueden haber mas de una que cumpla con las caracteristicas
		for (int i=0; i<this.Fichas.length; i++){
			
			if (this.Fichas[i] != null){//evaluamos que la posicion no es vacia y comparamos las alturas llamando al metodo que muestra la informacion
				if (this.Fichas[i].getAltura()< ficha.getAltura()){
					this.Fichas[i].MostrarFicha();
					cont=1;
				}
			}
		}
		
		if (cont==0)
			System.out.println("No se ha encontrado ninguna ficha en el container con altura menor a " + ficha.getAltura());
	}
	
	//metodo para ingresar una ficha, recibe una fecha, busca una posicion vacia y lo ingresa, muestra mensaje en caso de estar lleno el array
	public void IngresarFicha(FichaPaciente ficha){
		
		int cont=0;//para evaluar si esta lleno el array
		for (int i=0; i<this.Fichas.length; i++){
			
			if (this.Fichas[i] == null){
				this.Fichas[i] = ficha;//asignacion
				cont=1;
				break;
			}
		}
		
		if (cont==0){
			System.out.println("Container lleno!!!");
		}
	}
}