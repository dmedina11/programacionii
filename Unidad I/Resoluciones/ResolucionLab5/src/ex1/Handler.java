package ex1;

//clase que tiene la responsabilidad de simular el juego, evaluando el numero de los jugadores versus
//el numero del pc...
public class Handler {

	//metodo que permite la simulacion del juego, recibe una coleccion del tipo Player con los jugadores, en la primera posicion siempre esta el PC
	public void SimulationGame(Player [] players){
	
		//hacer jugada jugadores
		for (int i=0; i<players.length; i++)
			this.Jugada(players[i]);
		
		//comparar los resultados
		for (int i=1; i<players.length; i++){
			if (this.CompareNumber(players[0], players[i]))
				System.out.println("Player " + players[i].getName() + " Acerto!!!");
			else
				System.out.println("Player " + players[i].getName() + " Fallo!!!");
		}
	}
	
	//metodo que recibe una coleccion de jugadores y genera los valores de los numeros...
	private void Jugada(Player jugador){
		
		if (jugador.getKind() == 0)
			jugador.generateNumberPC();
		else
			jugador.generateNumberPlayer();
	}
	
	//metodo que retorna true o false dependiendo del valor de la comparacion de los numeros...
	private boolean CompareNumber(Player pc, Player jugador){
		
		System.out.println("Comparando jugadas: ");
		System.out.print(pc.getName() + " = " + pc.getNumber()+ " v/s ");
		System.out.println(jugador.getName() + " = " + jugador.getNumber());
		if (pc.getNumber() == jugador.getNumber())
			return true;
		else
			return false;
	}
}
