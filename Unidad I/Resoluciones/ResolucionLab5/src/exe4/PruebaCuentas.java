package exe4;

//clase de testeo...
public class PruebaCuentas {

	public static void main (String [] args){
	
		Persona persona = new Persona(18228843);//instancia a persona...
		//instancia a dos objetos de la clase Cuenta
		Cuenta c1 = new Cuenta(123, 0);
		Cuenta c2 = new Cuenta(234, 700000);
		
		//agregar las cuentas a la persona...
		persona.AddCuenta(c1);
		persona.AddCuenta(c2);
		
		//agregar monto a cuenta c1
		persona.getCuentas()[0].Abonar(1100);
		persona.getCuentas()[0].ConsultarSaldo();
		
		//pagar alquiler con cuenta c2
		System.out.println("Pagando alquiler");
		persona.getCuentas()[1].Pago(750000);
		persona.getCuentas()[1].ConsultarSaldo();
		persona.setMoroso(persona.EvaluarMorosidad());//evaluamos la morosidad del cliente
		System.out.print("Estado de morosidad: ");
		persona.MostrarMoroso();
		
		//transferencia
		System.out.println("Transfiriendo desde cuenta " + persona.getCuentas()[0].getNumero() + " a cuenta " + persona.getCuentas()[1].getNumero());
		persona.getCuentas()[1].Abonar(persona.getCuentas()[0].getSaldo());
		persona.getCuentas()[1].ConsultarSaldo();
		persona.getCuentas()[0].Pago(persona.getCuentas()[0].getSaldo());
		persona.getCuentas()[0].ConsultarSaldo();
	}
}
