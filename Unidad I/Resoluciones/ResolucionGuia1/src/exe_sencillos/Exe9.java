package exe_sencillos;

import java.util.Scanner;

public class Exe9 {

	public static void main (String [] args){
		
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("Ingrese grados celcius");
		
		double celcius = leer.nextDouble();
		double farenheit = (celcius*1.8)+32;
		System.out.println(celcius + " a grados Farenheit: " + farenheit);
	}
}
