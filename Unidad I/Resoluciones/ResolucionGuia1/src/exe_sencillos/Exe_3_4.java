package exe_sencillos;

import java.util.Scanner;

//representa los ejercicios 3 y 4
public class Exe_3_4 {

	public static void Calculo(int personas, int total){
		
		double iva = total * 0.19;
		double total_final = total+iva;
		double propina = total_final*0.1;
		double total_con_propina = total_final+propina;
		double a_pagar = total_con_propina/personas;
		
		Detalle(personas, total, total_final, a_pagar, iva, propina, total_con_propina);
	}
	
	public static void Detalle(int personas, int parcial, double total, double a_pagar, double iva, double propina, double total_propina){
		System.out.println("Detalle: ");
		System.out.println("Cantidad de personas: " + personas);
		System.out.println("Total Cuenta: "+ parcial);
		System.out.println("IVA: "+ iva);
		System.out.println("Total sin propina: " + total);
		System.out.println("Propina: " + total_propina);
		System.out.println("A pagar: " + a_pagar);
	}
	
	//solicitud de datos...
	public static void SolicitudDatos(){
		
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("Ingrese la cantidad de personas");
		int personas = leer.nextInt();
		System.out.println("Ingrese el total de la cuenta");
		int total = leer.nextInt();
		
		Calculo(personas, total);
	}
	public static void main (String [] args){
		
		//exe 3...
		Calculo(4, 10000);
		
		//exe4...
		SolicitudDatos();
	}
}
