package tarjetero;

//representa datos de personas con informacion...
public class Tarjetero {

	private Persona [] tarjetas;
	
	public Tarjetero(int cantidad) {
		this.tarjetas = new Persona[cantidad];
	}
	
	//agregar tarjetas...
	public void Agregar(Persona persona){
		
		int cont=0;
		for (int i=0; i<this.tarjetas.length; i++){
			
			if (this.tarjetas[i] == null){
				this.tarjetas[i]=persona;
				cont=1;
				break;
			}
		}
		
		if (cont==0)
			System.out.println("No hay espacio en el tarjetero!!!");
	}
	
	//imprimir tarjetas...
	public void Imprimir(){
		
		for (int i=0; i<this.tarjetas.length; i++){
			if (this.tarjetas[i]!= null)
				this.tarjetas[i].MostrarDatos();
		}
	}
}
