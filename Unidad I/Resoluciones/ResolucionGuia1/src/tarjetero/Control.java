package tarjetero;

import java.util.Scanner;

public class Control {

	public static Persona CrearPersona(){
		
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("Ingrese el nombre de la persona");
		String nombre = leer.next();
		System.out.println("Ingrese los apellidos");
		String apellidos = leer.next();
		System.out.println("Ingrese numero de telefono");
		int numero = leer.nextInt();
		System.out.println("Ingrese correo electronico");
		String correo = leer.next();
		System.out.println("Ingrese direccion postal");
		String postal = leer.next();
		
		return new Persona(nombre, apellidos, numero, correo, postal);
	}
	
	public static void main(String [] args){
		
		//testeamos con solo una tarjeta...
		Tarjetero tarjetero = new Tarjetero(2);
		tarjetero.Agregar(CrearPersona());
		tarjetero.Imprimir();
	}
}
