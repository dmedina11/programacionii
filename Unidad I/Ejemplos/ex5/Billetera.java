package ex05;

public class Billetera {
	
	private Tarjeta[] arregloTarj = new Tarjeta[4];
	

	public Tarjeta[] getArregloTarj() {
		return arregloTarj;
	}

	public void setArregloTarj(Tarjeta[] arregloTarj) {
		this.arregloTarj = arregloTarj;
	}
	
	public void addTarjeta(Tarjeta t){
		
		for (int i = 0 ; i < arregloTarj.length ; i++){
			
			if(arregloTarj[i]==null){
				
				arregloTarj[i] = t;
				break;
				
			}
			
		}
		
	}

	
	public boolean checkTarjeta(){
		
		
		for (int i = 0 ; i < arregloTarj.length; i ++){
			
			if (arregloTarj[i]!=null){
				
				if(arregloTarj[i].getLineaCredito() > 0){
					System.out.println("Tarjeta: "+arregloTarj[i].getNombre()+ " Tiene saldo positivo");
					return true;
				}
				
			}
			
		}
		
		return false;
	}	

}
