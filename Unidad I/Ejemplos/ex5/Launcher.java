package ex05;

public class Launcher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Tarjeta t = new Tarjeta();
		
		Tarjeta t1 =  new Tarjeta();
		
		Billetera b = new Billetera();
		
		
		t.setNombre("ChileCard");
		t.setLineaCredito(10000);
		
		t1.setNombre("BancoEstado");
		t1.setLineaCredito(200000);
		
		b.addTarjeta(t);
		b.addTarjeta(t1);
		
		/* Simulación de compra y venta:
		 * 
		 *  El usuario pueda comprar desde la linea de comando
		 *  Se escoja la tarjeta con saldo disponible. (revisar que uno no puede gasta mas de lo que tiene.!)
		 *  Cuando se me acaba el dinero disponible en las tarjetas.. se debe terminar la simulacion de compra.
		 *  Los montos asignados son al azar.
		 *  
		 *  */
		
		t.compra(1000);
		
		b.checkTarjeta();
		
		
	}

}
