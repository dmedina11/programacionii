package ex05;

public class Tarjeta {
	
	/*Atributos*/
	private String nombre;
	private int lineaCredito;
	
	/*Getters & Setters*/
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getLineaCredito() {
		return lineaCredito;
	}
	public void setLineaCredito(int lineaCredito) {
		this.lineaCredito = lineaCredito;
	}
	
	public void compra(int i){
		
		lineaCredito = lineaCredito - i;
		
	}
	
	
	

}
