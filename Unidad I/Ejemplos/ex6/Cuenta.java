
public class Cuenta {
	
	private String numeroCuenta;
	private int saldo;
	
	/*  Constructor*/
	
	public Cuenta(String nc,int s){
		
		numeroCuenta = nc;
		saldo = s;
			
	}
	
	public Cuenta(){
		
		numeroCuenta = "000000000";
		saldo = 0;
			
	}
	
	
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	
	
	

}
