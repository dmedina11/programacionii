
public class Persona {
	
	
	private String rut;
	private Cuenta[] arregloCuentas;
	private boolean moroso;
	
	
	/*Constructor*/
	
	public Persona(String r){
		
		rut = r;
		arregloCuentas = new Cuenta[3];
		moroso = false;
	}

	
	public void addcuentas(Cuenta c){
		
		for(int i = 0; i < arregloCuentas.length ; i ++){
			
			 if (arregloCuentas[i] == null){
				arregloCuentas[i] = c;
				break;
			 }
		}
		
	}
	
	public boolean revisarMoroso(){
		
		for(int i = 0; i < arregloCuentas.length ; i ++){
			
			 if (arregloCuentas[i] != null){
				 if(arregloCuentas[i].getSaldo() < 0){
					 moroso = true;
					 return true;
				 }
			 }
		}
		moroso = false;
		return false;
	
	}
		
	public void pagar(String nc, int monto){
		
		for(int i = 0; i < arregloCuentas.length ; i ++){
			
			 if (arregloCuentas[i].getNumeroCuenta().equals(nc)){
				 
				 int saldo = arregloCuentas[i].getSaldo();
				 arregloCuentas[i].setSaldo(saldo-monto);
				 break;
			 }
		
		}
	}


	public String getRut() {
		return rut;
	}
	
	public void muestraCuentas(){
		
		for(int i = 0; i < arregloCuentas.length ; i ++){
			
			 if (arregloCuentas[i]!=null){
				 System.out.println("El numero de cuenta es:"+ arregloCuentas[i].getNumeroCuenta());
				 System.out.println("El saldo es:"+ arregloCuentas[i].getSaldo());
			 }
		}		 
		
	}
	
	/*Implementar este metodo, recibiendo Numero de cuenta.*/
	public void transferir(Cuenta c1,Cuenta c2,int monto){
		
		if(c1.getSaldo() >= monto){
			/*Realizamos el deposito*/
			int saldo = c2.getSaldo();
			c2.setSaldo(saldo+monto);
			/*Sacamos los fondos*/
			saldo = c1.getSaldo();
			c1.setSaldo(saldo-monto);
			
		}else{
			
			System.out.println("Sr/a. Usted no puede realizar esta transferencia - SIN FONDO");
			
		}
				
	}
	
}
