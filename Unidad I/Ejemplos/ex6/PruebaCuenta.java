
public class PruebaCuenta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Persona p =  new Persona("18475962");
		Cuenta c1 = new Cuenta();
		Cuenta c2 =  new Cuenta("24500-03",700);

		
		p.addcuentas(c1);
		p.addcuentas(c2);
		
		c1.setSaldo(1100);
		
		System.out.println("La Persona rut: "+p.getRut()+" Tiene asociada las siguientes cuentas:" );
		p.muestraCuentas();
		
		p.pagar("24500-03", 750);
		
		if(p.revisarMoroso()){
			System.out.println("La persona rut: "+p.getRut()+" ES MOROSA");
		}else{
			System.out.println("La persona rut: "+p.getRut()+" NO ES MOROSA");	
		}
		
		p.muestraCuentas();
		
		p.transferir(c1, c2, 100);
		
		p.muestraCuentas();
		
		p.transferir(c2, c1, 1000);
		
		
		

	}

}
