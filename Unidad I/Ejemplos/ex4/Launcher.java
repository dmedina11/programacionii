package ex04;

public class Launcher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		/*Instaciamos una veterinaria*/
		Veterinaria v = new Veterinaria();
		v.setNombre("MundoAnimal");
		
		/*Creamos 3 gatos*/
		Gato g = new Gato();
		Gato g2 = new Gato();
		Gato g3 = new Gato();
		
		/*Asignamos estados a los objetos Gatos*/
		g.setEdad(3);
		g.setPeso(3);
		g.setNombre("Tigre");
		
		
		g2.setEdad(2);
		g2.setPeso(4);
		g2.setNombre("Chancho");
		
		
		g3.setEdad(5);
		g3.setPeso(4);
		g3.setNombre("Neko");
		
		/*Agregamos Gatos A la Veterinaria*/
		
		v.agregarGato(g);
		v.agregarGato(g2);
		v.agregarGato(g3);
		
		v.mostrarGato();

	
		System.out.println("El promedio de peso es:" +v.promedioPesoGato() );
	
	}
	
	
	

}
