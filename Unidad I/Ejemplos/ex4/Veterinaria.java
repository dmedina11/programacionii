package ex04;

import java.util.ArrayList;

public class Veterinaria {
	
	private String nombre;
	private ArrayList<Gato> g = new ArrayList<Gato>();
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ArrayList<Gato> getG() {
		return g;
	}
	public void setG(ArrayList<Gato> g) {
		this.g = g;
	}
	public void agregarGato(Gato gato){
		g.add(gato);
	}
	public int contarGatos(){
		return g.size();
	}
	public void mostrarGato(){
		
		for(int i = 0 ; i < g.size(); i++){
			
			System.out.println("Nombre: "+g.get(i).getNombre());
			System.out.println("Edad: "+g.get(i).getEdad());
			System.out.println("Peso: "+g.get(i).getPeso());
			
		}
		
	}
	public float promedioPesoGato(){
		
		float promedio = 0;
		
		for (int i = 0 ; i < g.size(); i++){
			
			promedio = promedio + g.get(i).getPeso();
			
		}
		
		return promedio/g.size();
	}
	
	
	
	

}
