package ex04;

public class Gato {

	private int peso;
	private int edad;
	private String nombre;
	
	
	public void setPeso(int p){
		peso = p;		
	}
	public void setEdad(int e){
		edad = e;
	}
	public void setNombre(String n){
		nombre = n;
	}
	public int getPeso() {
		return peso;
	}
	public int getEdad() {
		return edad;
	}
	public String getNombre() {
		return nombre;
	}
	
	
}
