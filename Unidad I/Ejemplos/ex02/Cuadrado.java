
public class Cuadrado {
	
	
	// Atributos
	
	public int lado;
	public int perimetro;
	public int area;
	
	// Métodos

	/*Opcion 1, sin return*/
	public void calcularArea(){
		
		area = lado * lado;
		
		
	}
	
	/*Opcion 2, con return*/
	public int calcularPerimetro(){
		
		perimetro =  lado * 4;
		return perimetro;
		
	}
	
	

}
