

// Visibilidad + tipo + Nombre
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			int perimetro;
			
			Cuadrado c = new Cuadrado();
			c.lado = 2;
			
			/* Opcion 1, se modifico el estado del objeto*/
			c.calcularArea();
			
			System.out.println("El area del cuadrado es: " + c.area);
			
			perimetro = c.calcularPerimetro();
			
			/*Opción 2, se retornó el estado del objeto*/
			System.out.println(perimetro);
			System.out.println(c.calcularPerimetro());
			
			
	}

}
