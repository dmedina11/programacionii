
public class Juego {

	public Persona p;
	public Dado d;
	
	
	public void Jugar(){
		
		int resultado = 0;
		/*Creación y asignación de objetos*/
		p = new Persona();
		d = new Dado();
		/*Asignación de valores a los objetos*/
		p.nombre = "Andres";
		d.lados = 20;
		
		resultado = p.lanzarDado(d);
		
		if(resultado == 7){
			System.out.println("El jugador: "+p.nombre+ " Gano el juego con numero: "+resultado);
		}else{
			System.out.println("El jugador: "+p.nombre+ " Perdio el juego con numero: "+d.numeroActual);
		}
		
		
	}
	
	
}
